﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;


namespace Mod_Starter
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        [assembly: AssemblyVersionAttribute("2.0.1")]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());

            /*
            name="Сервер Last Point: модификации от DZR";
            picture="dzr_lp_mods/image/avatar.tga";
            logo="dzr_lp_mods/image/avatar_s.tga";
            logoOver="dzr_lp_mods/image/avatar_s_o.tga";
            logoSmall="dzr_lp_mods/image/avatar_s.tga";
            tooltip="$STR_dzrlp_tooltip";
            tooltipOwned="$STR_dzrlp_tooltip";
            overview="$STR_dzrlp_overview";
            action="https://dayzrussia.com/f/index.php?threads/4410/#post-97840";
            author="$DayzRussia";
            version="0.8";
             * 
             * */

        }
    }
}
